##指标和测试要求
单目标的轨迹跟踪，最远测试距离4.8米，只跟踪最近的目标。 
        
##硬件要求
BGT60TR13评估板，照片如下![评估版照片](imag/evb.png)
##操作步骤
1. 固定雷达，保证雷达高度最好与胸口齐平，切不可用USB线悬挂雷达。
![雷达固定照片](imag/install.jpg)
2. 通过USB线缆将雷达连接的电脑，雷达评估板将BGT60TR13的ADC数据通过USB口上传到笔记本电脑，在笔记本电上通过运行python程序进行算法分析
![雷达连接](imag/connect.png)
3. 解压Presen_Detection_Pro到本地，点击main文件夹中，main.exe 
![软件](imag/main.png)
1. 启动后会现出现一个黑窗口，和一个轨迹显示窗口，如下图   
![结果](imag/result.png)
在雷达前运动即可生成轨迹


##其他注意问题
远距离测试需要注意，调整雷达角度，保证胸腔和雷达处于同一水平线，调整雷达角度微微上扬，如下图所示
![](./imag/angle.PNG)

##下载失败
请移步链接：https://pan.baidu.com/s/13zFNyZHdDW4Pjr0M0_XR4g 
提取码：0000